import 'package:flutter_getting_started/data/weather.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
class HttpHelper {
  final String apiKey = "1702b8a862bf757d1990cc83927a72cf";
  final String path = "data/2.5/weather";
  final String authority = "api.openweathermap.org";

  Future<Weather> getWeather(String location) async {
    Map<String, dynamic> parameters = {'q': location, 'appId': apiKey};
    Uri uri = Uri.https(authority, path, parameters);
    http.Response result = await http.get(uri);
    Map<String, dynamic> data = jsonDecode(result.body);
    Weather weather = Weather.fromJson(data);
    return weather;
  }
}
