import 'package:flutter/material.dart';
import 'package:flutter_getting_started/screens/bmi_screen.dart';
import 'package:flutter_getting_started/screens/intro_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      routes: {
        '/': (context) => const IntroScreen(),
        '/bmi': (coontext) => const BmiScreen()
      },
      initialRoute: '/',
    );
  }
}
