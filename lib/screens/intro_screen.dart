import 'package:flutter/material.dart';
import 'package:flutter_getting_started/shared/menu_bottom.dart';
import 'package:flutter_getting_started/shared/menu_drawer.dart';

class IntroScreen extends StatelessWidget {
  const IntroScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Intro screen'),
      ),
      drawer: const MenuDrawer(),
      body: const Center(
        child: FlutterLogo(),
      ),
      bottomNavigationBar: const MenuBottom(),
    );
  }
}
